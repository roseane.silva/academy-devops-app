package com.academy.devops.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.academy.devops.model.Person;
import com.academy.devops.service.PersonService;

@RestController
public class PersonController {

	private PersonService personService;
	
	@Autowired
	public PersonController(PersonService personService) {
		this.personService = personService;
	}
	
	@GetMapping(value = "/people")
	public List<Person> getAllPeople() {
		return this.personService.getPeople();
	}
	
}
