package com.academy.devops.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "person")
public class Person {
	@Id
	private String id;
	private String name;
	private Integer age;
	private String hobby;
	
	public Person(String name, Integer age, String hobby) {
		this.name = name;
		this.age = age;
		this.hobby = hobby;
	}
}
