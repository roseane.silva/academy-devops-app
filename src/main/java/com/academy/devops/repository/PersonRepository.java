package com.academy.devops.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.academy.devops.model.Person;

@Repository
public interface PersonRepository extends MongoRepository<Person, String> {

}
