# academy-devops-app

This App was created for the AC Academy DevOps training. Its main purpose is to show the connectivity between applications using Docker.

This App has a single endpoint `GET /people` which will retrieve all documents contained on a Mongo database, already initialized with a few records.

You don't need to worry about building the project, this App's Dockerfile uses multi-stage build, meaning it will build a Gradle image to build the project and copy the generated `.jar` file to the `build/libs` directory. 

**How to run this app:**

- Open Play with Docker
- Create a new instance

**Clone this project:**

`git clone https://gitlab.com/roseane.silva/academy-devops-app.git`

**Move to the project directory:**

`cd academy-devops-app/`

**Run docker compose**:

`docker-compose up -d`

**As soon as they're up, make a curl request:**

```
curl --request GET \
  --url http://localhost:8080/people
```

You should see these records (along with their randonly generated IDs): https://gitlab.com/roseane.silva/academy-devops-db/-/blob/main/setup_db.js#L6
